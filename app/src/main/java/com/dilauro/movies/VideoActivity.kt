package com.dilauro.movies

import android.os.Bundle
import android.widget.Toast
import com.dilauro.movies.databinding.ActivityVideoBinding
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView

class VideoActivity : YouTubeBaseActivity() {

    private val key = "AIzaSyB7zFoc7aCudvkjR7iWySsizb9Rro2aipQ"
    lateinit var youTubePlayer: YouTubePlayerView
    lateinit var binding: ActivityVideoBinding
    private lateinit var youtubePlayerInit: YouTubePlayer.OnInitializedListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        youTubePlayer = binding.ytPlayer
        val videoId = intent.getStringExtra("video_id")


        youtubePlayerInit = object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, p1: YouTubePlayer?, p2: Boolean) {
                p1?.loadVideo(videoId)
            }

            override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
                Toast.makeText(applicationContext, getString(R.string.youtube_failed), Toast.LENGTH_LONG).show()
            }
        }
        youTubePlayer.initialize(key, youtubePlayerInit)
    }

}