package com.dilauro.movies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.databinding.ActivityStartScreenBinding
import java.util.*

class StartScreen : AppCompatActivity() {
    private lateinit var binding: ActivityStartScreenBinding
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartScreenBinding.inflate(layoutInflater)
        CURRENT_LANG = Locale.getDefault().language

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)  // передаем в navСontroller fragment из layout
        binding.bottomNav.setupWithNavController(navController) // синхронизируем нижнюю панель с navController
        NavigationUI.setupActionBarWithNavController(this, navController)  // устанавливаем actionbar с navController
    }

    // переопределяем onSupportNavigateUp и передаем в нее наш контрроллер
    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, null)
    }
}