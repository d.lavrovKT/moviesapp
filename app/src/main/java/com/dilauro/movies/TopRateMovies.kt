package com.dilauro.movies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.dilauro.movies.adapters.MoviesListAdapter
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.ActivityTopRateMoviesBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TopRateMovies : AppCompatActivity() {
    lateinit var moviesListAdapter: MoviesListAdapter
    lateinit var binding: ActivityTopRateMoviesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTopRateMoviesBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun refreshMoviesList() {
        binding.moviesList.adapter = moviesListAdapter
    }

}