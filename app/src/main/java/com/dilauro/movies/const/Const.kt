package com.dilauro.movies.const

const val PRIVATE_API_KEY = "28516f332b8e77371711ee8977c5a0e3"
var CURRENT_LANG = "ru"

const val DB_NAME = "favoritesTable"
const val DB_VERSION = 1
const val DB_TABLE_NAME = "favorites"
const val COL_ITEM_ID = "itemID"
const val COL_ITEM_NAME = "itemName"
const val COL_ITEM_RATE = "itemRate"
const val COL_ITEM_POSTER_PATH = "itemPosterPath"
const val COL_ITEM_BACK_PATH = "itemBackPath"
const val COL_ITEM_IS_WATCHED = "itemIsWatched"