package com.dilauro.movies.api

import com.dilauro.movies.data.film.MovieItem
import com.dilauro.movies.data.moviepeople.Movie
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.data.person.PeopleList
import com.dilauro.movies.data.personInfo.PersonInfo
import com.dilauro.movies.data.personmovies.PersonMoviesList
import com.dilauro.movies.data.similar.Similar
import com.dilauro.movies.data.videos.Videos
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/popular")
    fun getMovies(@Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<MoviesList>

    @GET("movie/top_rated")
    fun getRateMovies(@Query("api_key") privateKey: String, @Query("language") currentLang: String, @Query("page") currentPage: Int): Call<MoviesList>

    @GET("movie/now_playing")
    fun getNowCinemaMovies(@Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<MoviesList>

    @GET("movie/upcoming")
    fun getNewCinemaMovies(@Query("api_key") privateKey: String, @Query("language") currentLang: String, @Query("region") location: String): Call<MoviesList>

    @GET("person/popular")
    fun getPopularPerson(@Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<PeopleList>

    @GET("movie/{movie_id}")
    fun getMovieDetail(@Path("movie_id") movieId: Int, @Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<MovieItem>

    @GET("movie/{movie_id}/videos")
    fun getVideosLink(@Path("movie_id") movieId: Int, @Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<Videos>

    @GET("movie/{movie_id}")
    fun getMoviesWhitCredits(
        @Path("movie_id") movieId: Int,
        @Query("api_key") privateKey: String,
        @Query("language") currentLang: String,
        @Query("append_to_response") append_to_response: String
    ): Call<Movie>

    @GET("movie/{movie_id}/similar")
    fun getSimilarMovies(@Path("movie_id") movieId: Int, @Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<Similar>

    @GET("person/{person_id}")
    fun getDetailPersonInfo(@Path("person_id") personId: Int, @Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<PersonInfo>

    @GET("person/{person_id}/movie_credits")
    fun getPersonMovies(@Path("person_id") personId: Int, @Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<PersonMoviesList>

    @GET("movie/top_rated")
    fun getRandomMovie(@Query("page") randomPage: Int, @Query("api_key") privateKey: String, @Query("language") currentLang: String): Call<MoviesList>

    @GET("search/movie")
    fun getSearchResult(@Query("api_key") privateKey: String, @Query("language") currentLang: String, @Query("query") searchCall: String): Call<MoviesList>

    companion object {
        var BASE_URL = "https://api.themoviedb.org/3/"

        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }
}