package com.dilauro.movies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.dilauro.movies.adapters.PersonMoviesAdapter
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.personInfo.PersonInfo
import com.dilauro.movies.data.personmovies.PersonMoviesList
import com.dilauro.movies.databinding.ActivityDetailPersonBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPersonActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailPersonBinding
    private lateinit var personMoviesAdapter: PersonMoviesAdapter
    private val replaceDataValue = ReplaceDataValue(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailPersonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getPersonDetailInfo()
        getPersonMovies()
        binding.btnBack.setOnClickListener {
            finish()
        }
    }

    private fun getPersonDetailInfo() {
        val personId = intent.getIntExtra("person_id", 0)
        val apiServiceDetailPerson = ApiService.create().getDetailPersonInfo(personId, PRIVATE_API_KEY, CURRENT_LANG)

        apiServiceDetailPerson.enqueue(object : Callback<PersonInfo> {
            override fun onResponse(call: Call<PersonInfo>, response: Response<PersonInfo>) {
                binding.personName.text = response.body()?.name
                if (response.body()?.place_of_birth != null) {
                    binding.placeOfBirth.text = response.body()?.place_of_birth
                } else {
                    binding.placeOfBirth.visibility = View.INVISIBLE
                    binding.labelPlaceOfBirth.visibility = View.INVISIBLE
                }

                if (response.body()?.birthday != null) {
                    binding.dateOfBirth.text = response.body()?.birthday?.let { replaceDataValue.replaceDate(it) }
                } else {
                    binding.dateOfBirth.visibility = View.INVISIBLE
                    binding.labelDateOfBirth.visibility = View.INVISIBLE
                }
                if (response.body()?.deathday != null) {
                    binding.deathOfDate.text = replaceDataValue.replaceDate(response.body()?.deathday.toString())
                    binding.deathOfDate.visibility = View.VISIBLE
                    binding.deathTitle.visibility = View.VISIBLE
                }
                if (response.body()?.biography != "") {
                    binding.biographyInfo.text = response.body()?.biography
                } else {
                    binding.divBiography.visibility = View.INVISIBLE
                }
                if (response.body()?.profile_path != null) {
                    Picasso.get().load("https://images.tmdb.org/t/p/w500" + response.body()?.profile_path).into(binding.profileImg)
                } else {
                    binding.profileImg.setImageResource(R.drawable.no_image)
                }
            }

            override fun onFailure(call: Call<PersonInfo>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun refreshPersonMovieList() {
        binding.personMovieRecycler.adapter = personMoviesAdapter
    }

    private fun getPersonMovies() {
        val personId = intent.getIntExtra("person_id", 0)
        val apiServicePersonMovies = ApiService.create().getPersonMovies(personId, PRIVATE_API_KEY, CURRENT_LANG)

        apiServicePersonMovies.enqueue(object : Callback<PersonMoviesList> {
            override fun onResponse(call: Call<PersonMoviesList>, response: Response<PersonMoviesList>) {
                if (response.body()?.cast?.size!! < 5) {
                    binding.personMovieRecycler.minimumHeight = 200
                }
                personMoviesAdapter = response.body()?.let { PersonMoviesAdapter(it, this@DetailPersonActivity) }!!
                refreshPersonMovieList()
                binding.personMovieRecycler.layoutManager = GridLayoutManager(this@DetailPersonActivity, 1)
            }

            override fun onFailure(call: Call<PersonMoviesList>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}