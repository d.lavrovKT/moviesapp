package com.dilauro.movies

import android.content.Context
import android.net.wifi.WifiManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.Formatter
import com.dilauro.movies.databinding.ActivityRussiaLocationBinding

class RussiaLocationActivity : AppCompatActivity() {
    private lateinit var binding : ActivityRussiaLocationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRussiaLocationBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}