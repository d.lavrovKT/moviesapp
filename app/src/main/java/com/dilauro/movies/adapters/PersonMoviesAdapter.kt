package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.data.personmovies.Cast
import com.dilauro.movies.data.personmovies.PersonMoviesList
import com.dilauro.movies.databinding.PersonMovieItemBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso

class PersonMoviesAdapter(private val personMoviesList: PersonMoviesList, val context: Context) : RecyclerView.Adapter<PersonMoviesAdapter.PersonMoviesHolder>() {

    private val replaceDataValue = ReplaceDataValue(context)
    private val sortCastList = personMoviesList.cast.sortedByDescending { it.release_date }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonMoviesHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.person_movie_item, parent, false)
        return PersonMoviesHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PersonMoviesHolder, position: Int) {
        holder.movieTitle.text = sortCastList[position].title
        if ((sortCastList[position].release_date) != "") {
            holder.movieYear.text = replaceDataValue.getYearDate(sortCastList[position].release_date)
        } else {
            holder.movieYear.visibility = View.GONE
        }
        if (sortCastList[position].vote_average != 0.0) {
            holder.movieRate.text = sortCastList[position].vote_average.toString()
        } else {
            holder.rowRate.visibility = View.GONE
        }
        if (sortCastList[position].character != "") {
            holder.movieCharacter.text = sortCastList[position].character
        } else {
            holder.movieCharacter.visibility = View.GONE
        }
        if (sortCastList[position].poster_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + (sortCastList[position].poster_path)).into(holder.moviePoster)
        } else if (sortCastList[position].backdrop_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + (sortCastList[position].backdrop_path)).into(holder.moviePoster)
        } else {
            holder.moviePoster.setImageResource(R.drawable.no_image)
        }
        holder.movie.setOnClickListener {
            val intent = Intent(holder.movie.context, DetailMovieActivity::class.java)
            intent.putExtra("movie_id", sortCastList[position].id)
            holder.movie.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return personMoviesList.cast.size
    }

    class PersonMoviesHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = PersonMovieItemBinding.bind(item)
        val movie = binding.personMovieDiv
        val movieTitle = binding.movieTitle
        val moviePoster = binding.movieImg
        val movieYear = binding.movieYear
        val movieRate = binding.movieRate
        val rowRate = binding.rateLinear
        val movieCharacter = binding.movieCharacter

    }
}