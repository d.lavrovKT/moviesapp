package com.dilauro.movies.adapters.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.dilauro.movies.const.*
import com.dilauro.movies.data.dbclasses.FavoritesItem

class DbHandler(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val createFavoritesTable = "CREATE TABLE $DB_TABLE_NAME (" +
                "$COL_ITEM_ID integer PRIMARY KEY," +
                "$COL_ITEM_NAME string," +
                "$COL_ITEM_RATE integer," +
                "$COL_ITEM_POSTER_PATH string," +
                "$COL_ITEM_BACK_PATH string," +
                "$COL_ITEM_IS_WATCHED varchar);"

        db.execSQL(createFavoritesTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    fun addToFavoriteItem(item: FavoritesItem): Boolean {
        val db = writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_ITEM_ID, item.itemId)
        contentValues.put(COL_ITEM_NAME, item.itemName)
        contentValues.put(COL_ITEM_RATE, item.itemRate)
        contentValues.put(COL_ITEM_POSTER_PATH, item.itemPosterPath)
        contentValues.put(COL_ITEM_BACK_PATH, item.itemBackPath)
        contentValues.put(COL_ITEM_IS_WATCHED, item.itemIsWatched)
        val result = db.insert(DB_TABLE_NAME, null, contentValues)
        return result != (-1).toLong()
    }

    @SuppressLint("Recycle", "Range")
    fun getFavoriteListId(): ArrayList<Int> {
        val result = arrayListOf<Int>()
        val db = readableDatabase
        val queryResult = db.rawQuery("SELECT * from $DB_TABLE_NAME", null)
        if (queryResult.moveToFirst()) {
            do {
                val itemId = queryResult.getInt(queryResult.getColumnIndex(COL_ITEM_ID))
                result.add(itemId)
            } while (queryResult.moveToNext())
        }

        queryResult.close()
        return result

    }

    @SuppressLint("Recycle", "Range")
    fun getToFavoriteItem(state: Int): MutableList<FavoritesItem> {
        val result: MutableList<FavoritesItem> = ArrayList()
        val db = readableDatabase
        val queryResult = db.rawQuery("SELECT * from $DB_TABLE_NAME WHERE $COL_ITEM_IS_WATCHED=$state", null)
        if (queryResult.moveToFirst()) {
            do {
                val item = FavoritesItem()
                item.itemId = queryResult.getInt(queryResult.getColumnIndex(COL_ITEM_ID))
                item.itemName = queryResult.getString(queryResult.getColumnIndex(COL_ITEM_NAME))
                item.itemRate = queryResult.getDouble(queryResult.getColumnIndex(COL_ITEM_RATE))
                item.itemPosterPath = queryResult.getString(queryResult.getColumnIndex(COL_ITEM_POSTER_PATH))
                item.itemBackPath = queryResult.getString(queryResult.getColumnIndex(COL_ITEM_BACK_PATH))
                item.itemIsWatched = queryResult.getInt(queryResult.getColumnIndex(COL_ITEM_IS_WATCHED))
                result.add(item)
            } while (queryResult.moveToNext())
        }

        queryResult.close()
        return result
    }

    fun deleteToFavoritesItem(itemId: Int) {
        val db = writableDatabase
        db.delete(DB_TABLE_NAME, "$COL_ITEM_ID=?", arrayOf(itemId.toString()))
    }

    @SuppressLint("Recycle", "Range")
    fun updateFavoriteItemState(itemId: Int, state: Int) {
        val db = writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_ITEM_IS_WATCHED, state)
        db.update(DB_TABLE_NAME, contentValues, "$COL_ITEM_ID=?", arrayOf(itemId.toString()))
    }
}