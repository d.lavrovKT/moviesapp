package com.dilauro.movies.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.R
import com.dilauro.movies.adapters.posterfragments.PosterFragmentsView
import com.dilauro.movies.data.movies.Movie
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.SearchResultItemBinding


class TopListAdapter(private val moviesList: MoviesList, val context: Context) : RecyclerView.Adapter<TopListAdapter.TopListHolder>() {

    private val posterFragmentsView = PosterFragmentsView(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.search_result_item, parent, false)
        return TopListHolder(view)
    }

    override fun getItemCount(): Int {
        return moviesList.results.size
    }

    override fun onBindViewHolder(holder: TopListHolder, position: Int) {
        posterFragmentsView.fragmentViewPrint(moviesList.results[position], holder.binding)
    }

    class TopListHolder(item: View) : RecyclerView.ViewHolder(item) {
        val binding = SearchResultItemBinding.bind(item)
        val movieTitle = binding.titleResultItem
    }

    fun updateItems (newItems : MutableList<Movie>) {
        Log.d("UPDATE_REC", moviesList.results.size.toString())
        this.moviesList.results.addAll(newItems)
        Log.d("UPDATE_REC", moviesList.results.size.toString())
    }
}