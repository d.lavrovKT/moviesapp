package com.dilauro.movies.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailPersonActivity
import com.dilauro.movies.R
import com.dilauro.movies.data.moviepeople.Movie
import com.dilauro.movies.databinding.CastMovieItemBinding
import com.squareup.picasso.Picasso

class CastListAdapter(private val movie: Movie) : RecyclerView.Adapter<CastListAdapter.CastListHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cast_movie_item, parent, false)
        return CastListHolder(view)
    }

    override fun onBindViewHolder(holder: CastListHolder, position: Int) {
        holder.personName.text = movie.credits.cast[position].name
        if (movie.credits.cast[position].character != "") {
            holder.personRole.text = movie.credits.cast[position].character
        } else {
            holder.personRole.visibility = View.INVISIBLE
        }
        if (movie.credits.cast[position].profile_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + movie.credits.cast[position].profile_path).into(holder.personImg)
        } else {
            holder.personImg.setImageResource(R.drawable.no_image)
        }
        holder.personContainer.setOnClickListener {
            val intent = Intent(holder.personContainer.context, DetailPersonActivity::class.java)
            intent.putExtra("person_id", movie.credits.cast[position].id)
            holder.personContainer.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return movie.credits.cast.size
    }

    class CastListHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = CastMovieItemBinding.bind(item)
        val personContainer = binding.profileItem
        val personName = binding.actorName
        val personImg = binding.profileImg
        val personRole = binding.actorRole
    }
}