package com.dilauro.movies.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailPersonActivity
import com.dilauro.movies.R
import com.dilauro.movies.data.moviepeople.Movie
import com.dilauro.movies.databinding.MoviePeoplesItemBinding
import com.squareup.picasso.Picasso

class MoviePersonAdapter(private val movie: Movie) : RecyclerView.Adapter<MoviePersonAdapter.MoviePersonHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviePersonHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_peoples_item, parent, false)
        return MoviePersonHolder(view)
    }

    override fun onBindViewHolder(holder: MoviePersonHolder, position: Int) {
        holder.personName.text = movie.credits.cast[position].name
        if (movie.credits.cast[position].character != "") {
            holder.personRole.text = movie.credits.cast[position].character
        } else {
            holder.personRole.visibility = View.INVISIBLE
        }
        if (movie.credits.cast[position].profile_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + movie.credits.cast[position].profile_path).into(holder.personImg)
        } else {
            holder.personImg.setImageResource(R.drawable.no_image)
        }
        holder.personBody.setOnClickListener {
            val intent = Intent(holder.personImg.context, DetailPersonActivity::class.java)
            intent.putExtra("person_id", movie.credits.cast[position].id)
            holder.personImg.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return if (movie.credits.cast.size < 6) {
            movie.credits.cast.size
        } else 6
    }

    class MoviePersonHolder(item: View) : RecyclerView.ViewHolder(item) {
        private var binding = MoviePeoplesItemBinding.bind(item)
        val personBody = binding.personBody
        val personName = binding.profileName
        val personImg = binding.profileImg
        val personRole = binding.profileRole
    }
}