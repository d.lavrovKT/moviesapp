package com.dilauro.movies.adapters

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import com.dilauro.movies.VideoActivity
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.videos.Videos
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StartYoutubeVideo(val context: Context) {

    fun startVideo(movieId: Int, view: View) {
        val apiServiceMovieVideo = ApiService.create().getVideosLink(movieId, PRIVATE_API_KEY, CURRENT_LANG)
        apiServiceMovieVideo.enqueue(object : Callback<Videos> {
            override fun onResponse(call: Call<Videos>, response: Response<Videos>) {
                if (response.body() != null && response.body()!!.results.isNotEmpty()) {
                    response.body()?.results?.get(0)?.let { it1 -> onClickViewVideo(view, it1.key) }
                } else {
                    Toast.makeText(context, "Не удалось загрузить трейлер", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Videos>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun onClickViewVideo(view: View, videoId: String) {
        val intent = Intent(view.context, VideoActivity::class.java)
        intent.putExtra("video_id", videoId)
        view.context.startActivity(intent)
    }
}