package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.film.MovieItem
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.MoviesListItemBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesListAdapter(private val moviesList: MoviesList, val context: Context) : RecyclerView.Adapter<MoviesListAdapter.MoviesHolder>() {

    val replaceDataValue = ReplaceDataValue(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movies_list_item, parent, false)
        return MoviesHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MoviesHolder, position: Int) {
        holder.movieTitle.text = moviesList.results[position].title

        val apiServiceMovieItem = ApiService.create().getMovieDetail(moviesList.results[position].id, PRIVATE_API_KEY, CURRENT_LANG)
        apiServiceMovieItem.enqueue(object : Callback<MovieItem> {
            override fun onResponse(call: Call<MovieItem>, response: Response<MovieItem>) {
                if (response.body() != null && response.body()?.genres?.size!! > 0)
                    holder.movieVoteAver.text = (response.body()?.genres?.get(0)?.name ?: "") +
                            if (response.body()?.release_date != null) {
                                ", " + replaceDataValue.getYearDate(response.body()?.release_date.toString())
                            } else {
                                ""
                            }
            }

            override fun onFailure(call: Call<MovieItem>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
        if (moviesList.results[position].poster_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + moviesList.results[position].poster_path).into(holder.moviePoster)
        } else {
            holder.moviePoster.setImageResource(R.drawable.no_image)
        }

        if (moviesList.results[position].vote_average != 0.0) {
            holder.rowRateVote.text = moviesList.results[position].vote_average.toString()
        } else {
            holder.rowRate.visibility = View.INVISIBLE
        }


        holder.movie.setOnClickListener {
            val intent = Intent(holder.movie.context, DetailMovieActivity::class.java)
            intent.putExtra("movie_id", moviesList.results[position].id)
            holder.movie.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return moviesList.results.size
    }

    class MoviesHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = MoviesListItemBinding.bind(item)
        val movie = binding.moviesLayout
        val movieTitle = binding.movieTitle
        val moviePoster = binding.movieImg
        val movieVoteAver = binding.movieVoteAver
        val rowRate = binding.rawRate
        val rowRateVote = binding.rawRateVote
    }


}