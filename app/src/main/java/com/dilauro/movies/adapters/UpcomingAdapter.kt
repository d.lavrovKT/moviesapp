package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.VideoActivity
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.film.MovieItem
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.data.videos.Videos
import com.dilauro.movies.databinding.ActivityStartScreenBinding
import com.dilauro.movies.databinding.UpcomingItemBinding
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpcomingAdapter(private val moviesList: MoviesList) : RecyclerView.Adapter<UpcomingAdapter.MoviesHolder>() {

    private lateinit var startYoutubeVideo: StartYoutubeVideo

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.upcoming_item, parent, false)
        return MoviesHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MoviesHolder, position: Int) {
        holder.movieTitle.text = moviesList.results[position].title

        val apiServiceMovieItem = ApiService.create().getMovieDetail(moviesList.results[position].id, PRIVATE_API_KEY, CURRENT_LANG)
        apiServiceMovieItem.enqueue(object : Callback<MovieItem> {
            override fun onResponse(call: Call<MovieItem>, response: Response<MovieItem>) {
                if (response.body() != null && response.body()?.genres?.size!! > 0) {
                    holder.movieVoteAver.text = (response.body()?.genres?.get(0)?.name ?: "") +
                            if (response.body()?.genres?.size!! > 1) {
                                "/" + (response.body()?.genres?.get(1)?.name ?: "")
                            } else {
                                ""
                            }
                }
            }

            override fun onFailure(call: Call<MovieItem>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
        if (moviesList.results[position].backdrop_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + moviesList.results[position].backdrop_path).into(holder.moviePoster)
        } else if (moviesList.results[position].poster_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + moviesList.results[position].poster_path).into(holder.moviePoster)
        } else {
            holder.moviePoster.setImageResource(R.drawable.no_image)
        }

        holder.moviePoster.setOnClickListener {
            startYoutubeVideo = StartYoutubeVideo(holder.moviePoster.context)
            startYoutubeVideo.startVideo(moviesList.results[position].id, holder.movie)
        }

        holder.movieTitle.setOnClickListener {
            val intent = Intent(holder.movieTitle.context, DetailMovieActivity::class.java)
            intent.putExtra("movie_id", moviesList.results[position].id)
            holder.movie.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return moviesList.results.size
    }

    class MoviesHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = UpcomingItemBinding.bind(item)
        val movie = binding.moviesLayout
        val movieTitle = binding.movieTitle
        val moviePoster = binding.movieImg
        val movieVoteAver = binding.movieVoteAver
    }


}