package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.film.MovieItem
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.ReleaseListItemBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CalendarListMovieAdapter(private val movieList: MoviesList, val context: Context) : RecyclerView.Adapter<CalendarListMovieAdapter.CalendarListMovieHolder>() {

    private val replaceDataValue = ReplaceDataValue(context)
    private val sortDateMovieList = movieList.results.sortedBy { it.release_date }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarListMovieHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.release_list_item, parent, false)
        return CalendarListMovieHolder(view)
    }

    override fun getItemCount(): Int {
        return movieList.results.size
    }

    override fun onBindViewHolder(holder: CalendarListMovieHolder, position: Int) {
        with(sortDateMovieList[position]) {
            holder.movieTile.text = title
            holder.movieDay.text = replaceDataValue.getCalendarDay(release_date)
            holder.movieMonth.text = replaceDataValue.getCalendarMonth(release_date)
            if (backdrop_path != null) {
                Picasso.get().load("https://images.tmdb.org/t/p/w500$backdrop_path").into(holder.moviePoster)
            } else if (poster_path != null) {
                Picasso.get().load("https://images.tmdb.org/t/p/w500$poster_path").into(holder.moviePoster)
            } else {
                holder.moviePoster.setImageResource(R.drawable.no_image)
            }
            holder.movieDiv.setOnClickListener {
                val intent = Intent(holder.movieDiv.context, DetailMovieActivity::class.java)
                intent.putExtra("movie_id", id)
                holder.movieDiv.context.startActivity(intent)
            }

            val apiServiceMovieItem = ApiService.create().getMovieDetail(id, PRIVATE_API_KEY, CURRENT_LANG)
            apiServiceMovieItem.enqueue(object : Callback<MovieItem> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<MovieItem>, response: Response<MovieItem>) {
                    if (response.body()?.genres?.size!! > 0) {
                        holder.movieGenres.text = (response.body()?.genres?.get(0)?.name ?: "") +
                                if (response.body()?.genres?.size!! > 1) {
                                    "/" + (response.body()?.genres?.get(1)?.name ?: "")
                                } else {
                                    ""
                                }
                    } else {
                        holder.movieGenres.visibility = View.GONE
                    }
                }

                override fun onFailure(call: Call<MovieItem>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
        }
    }

    class CalendarListMovieHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = ReleaseListItemBinding.bind(item)
        val movieDiv = binding.movieDiv
        val movieTile = binding.movieTitle
        val moviePoster = binding.moviePoster
        val movieDay = binding.releaseDay
        val movieMonth = binding.releaseMonths
        val movieGenres = binding.movieGenres
    }
}