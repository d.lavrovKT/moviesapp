package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.R
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.data.person.PeopleList
import com.dilauro.movies.databinding.MoviesListItemBinding
import com.squareup.picasso.Picasso

class PeopleListAdapter(private val peopleList: PeopleList) : RecyclerView.Adapter<PeopleListAdapter.PeopleHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movies_list_item, parent, false)
        return PeopleHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PeopleHolder, position: Int) {
        holder.movieTitle.text = peopleList.results[position].name
        holder.movieVoteAver.text = peopleList.results[position].popularity.toString()
        Picasso.get().load("https://images.tmdb.org/t/p/w500" + peopleList.results[position].profile_path).into(holder.moviePoster)
    }

    override fun getItemCount(): Int {
        return peopleList.results.size
    }

    class PeopleHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = MoviesListItemBinding.bind(item)
        val movie = binding.moviesLayout
        val movieTitle = binding.movieTitle
        val moviePoster = binding.movieImg
        val movieVoteAver = binding.movieVoteAver
    }
}