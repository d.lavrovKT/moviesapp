package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.adapters.database.DbHandler
import com.dilauro.movies.data.dbclasses.FavoritesItem
import com.dilauro.movies.databinding.FavoriteListItemBinding
import com.dilauro.movies.screens.favorites.FavoritesFragment
import com.squareup.picasso.Picasso

class FavoriteListAdapter(private val favoritesList: MutableList<FavoritesItem>, private val fragment: FavoritesFragment, private val tabState: Int) :
    RecyclerView.Adapter<FavoriteListAdapter.FavoritesListHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.favorite_list_item, parent, false)
        return FavoritesListHolder(view)
    }

    override fun getItemCount(): Int {
        return favoritesList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: FavoritesListHolder, position: Int) {
        if (tabState != 0) {
            holder.watchedBtn.visibility = View.GONE
            holder.repeatBtn.visibility = View.VISIBLE
        }

        holder.name.text = favoritesList[position].itemName
        if (favoritesList[position].itemRate != 0.0) {
            holder.rate.text = favoritesList[position].itemRate.toString()
        } else {
            holder.rate.visibility = View.INVISIBLE
        }
        if (favoritesList[position].itemBackPath != "null") {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + favoritesList[position].itemBackPath).into(holder.poster)
        } else if (favoritesList[position].itemPosterPath != "null") {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + favoritesList[position].itemPosterPath).into(holder.poster)
        } else {
            holder.poster.setImageResource(R.drawable.no_image)
        }

        holder.moreInfoBtn.setOnClickListener {
            val intent = Intent(holder.moreInfoBtn.context, DetailMovieActivity::class.java)
            intent.putExtra("movie_id", favoritesList[position].itemId)
            holder.moreInfoBtn.context.startActivity(intent)
        }

        holder.deleteBtn.setOnClickListener {
            val dialog = AlertDialog.Builder(holder.deleteBtn.context)
            dialog.setTitle(R.string.delete_quetsion)
            dialog.setPositiveButton(R.string.delete) { _: DialogInterface, _: Int ->
                fragment.dbHandler.deleteToFavoritesItem(favoritesList[position].itemId)
                fragment.refreshFavoritesList(tabState)
            }
            dialog.setNegativeButton(R.string.cancel) { _: DialogInterface, _: Int ->
            }
            dialog.show()
        }

        holder.watchedBtn.setOnClickListener {
            val dialog = AlertDialog.Builder(holder.deleteBtn.context)
            dialog.setTitle(R.string.move_question)
            dialog.setPositiveButton(R.string.move) { _: DialogInterface, _: Int ->
                fragment.dbHandler.updateFavoriteItemState(favoritesList[position].itemId, 1)
                fragment.refreshFavoritesList(tabState)
            }
            dialog.setNegativeButton(R.string.cancel) { _: DialogInterface, _: Int ->
            }
            dialog.show()
        }

        holder.repeatBtn.setOnClickListener {
            val dialog = AlertDialog.Builder(holder.repeatBtn.context)
            dialog.setTitle(R.string.return_btn)
            dialog.setPositiveButton(R.string.return_btn_dialog) { _: DialogInterface, _: Int ->
                fragment.dbHandler.updateFavoriteItemState(favoritesList[position].itemId, 0)
                fragment.refreshFavoritesList(tabState)
            }
            dialog.setNegativeButton(R.string.cancel) { _: DialogInterface, _: Int ->
            }
            dialog.show()
        }
    }

    class FavoritesListHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = FavoriteListItemBinding.bind(item)
        val name = binding.movieTitle
        val poster = binding.favoritesPoster
        val rate = binding.labelRate
        val moreInfoBtn = binding.moreBtn
        val watchedBtn = binding.watchedBtn
        val deleteBtn = binding.deleteBtn
        val repeatBtn = binding.repeatBtn
    }
}