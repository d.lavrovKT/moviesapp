package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.data.similar.Similar
import com.dilauro.movies.databinding.MoviesListItemBinding
import com.dilauro.movies.databinding.SimilarMovieItemBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso

class SimilarListAdapter(private val similarList: Similar, val context: Context) : RecyclerView.Adapter<SimilarListAdapter.SimilarListHolder>() {

    val replaceDataValue = ReplaceDataValue(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimilarListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.similar_movie_item, parent, false)
        return SimilarListHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: SimilarListHolder, position: Int) {
        holder.movieTitle.text = similarList.results[position].title
        if (similarList.results[position].title.length > 22) {
            holder.movieTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12F)
        }
        if (similarList.results[position].vote_average != 0.0 || similarList.results[position].vote_average != 10.0) {
            holder.rateNum.text = replaceDataValue.replacePopularity(similarList.results[position].vote_average)
        } else {
            holder.rowRate.visibility = View.INVISIBLE
        }
        if (similarList.results[position].poster_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + similarList.results[position].poster_path).into(holder.moviePoster)
        } else {
            holder.moviePoster.setImageResource(R.drawable.no_image)
        }

        holder.movie.setOnClickListener {
            val intent = Intent(holder.movie.context, DetailMovieActivity::class.java)
            intent.putExtra("movie_id", similarList.results[position].id)
            holder.movie.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return similarList.results.size
    }

    class SimilarListHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = SimilarMovieItemBinding.bind(item)
        val movie = binding.moviesLayout
        val movieTitle = binding.movieTitle
        val moviePoster = binding.movieImg
        val rowRate = binding.similarRate
        val rateNum = binding.similarRateNum
    }
}