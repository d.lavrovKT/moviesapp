package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.R
import com.dilauro.movies.adapters.posterfragments.PosterFragmentsView
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.SearchResultItemBinding

class ResultSearchAdapter(private val moviesList: MoviesList, val context: Context) : RecyclerView.Adapter<ResultSearchAdapter.ResultMovieListHolder>() {

    private val posterFragmentsView = PosterFragmentsView(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultMovieListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.search_result_item, parent, false)
        return ResultMovieListHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ResultMovieListHolder, position: Int) {
        posterFragmentsView.fragmentViewPrint(moviesList.results[position], holder.binding)
    }

    override fun getItemCount(): Int {
        return moviesList.results.size
    }

    class ResultMovieListHolder(item: View) : RecyclerView.ViewHolder(item) {
        val binding = SearchResultItemBinding.bind(item)
        val movieTitle = binding.titleResultItem
    }
}