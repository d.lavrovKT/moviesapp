package com.dilauro.movies.adapters.posterfragments

import android.content.Context
import android.content.Intent
import android.view.View
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.data.movies.Movie
import com.dilauro.movies.databinding.SearchResultItemBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso

class PosterFragmentsView (val context: Context) {
    private val replaceDataValue = ReplaceDataValue(context)

    fun fragmentViewPrint (movie: Movie, holder: SearchResultItemBinding) {
        holder.titleResultItem.text = movie.title
        if (movie.poster_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500${movie.poster_path}").into(holder.searchMoviePoster)
        } else {
            holder.searchMoviePoster.setImageResource(R.drawable.no_image)
        }
        if ((movie.vote_average).toInt() != 0) {
            holder.labelRate.text = replaceDataValue.replacePopularity(movie.vote_average)
        } else {
            holder.labelRate.visibility = View.INVISIBLE
        }
        if (movie.release_date != null) {
            holder.titleResultYear.text = replaceDataValue.getYearDate(movie.release_date)
        } else {
            holder.titleResultYear.visibility = View.INVISIBLE
        }
        holder.searchMoviePoster.setOnClickListener {
            val intent = Intent(holder.searchMoviePoster.context, DetailMovieActivity::class.java)
            intent.putExtra("movie_id", movie.id)
            holder.searchMoviePoster.context.startActivity(intent)
        }
    }
}