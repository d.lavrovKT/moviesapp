package com.dilauro.movies.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.DetailPersonActivity
import com.dilauro.movies.R
import com.dilauro.movies.data.person.PeopleList
import com.dilauro.movies.databinding.PopularPersonItemBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso

class PopularPeopleAdapter(private val popularPersonList: PeopleList, val context: Context) : RecyclerView.Adapter<PopularPeopleAdapter.PopularPersonHolder>() {

    private val replaceDataValue = ReplaceDataValue(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularPersonHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.popular_person_item, parent, false)
        return PopularPersonHolder(view)
    }

    override fun getItemCount(): Int {
        return popularPersonList.results.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PopularPersonHolder, position: Int) {
        holder.personName.text = popularPersonList.results[position].name
        if (popularPersonList.results[position].profile_path != null) {
            Picasso.get().load("https://images.tmdb.org/t/p/w500" + popularPersonList.results[position].profile_path).into(holder.personImg)
        } else {
            holder.personImg.setImageResource(R.drawable.no_image)
        }
        if (popularPersonList.results[position].popularity != 0.0) {
            holder.personPopularity.text = popularPersonList.results[position].popularity.toInt().toString()
        } else {
            holder.rowPopularity.visibility = View.INVISIBLE
        }

        holder.personDiv.setOnClickListener {
            val intent = Intent(holder.personDiv.context, DetailPersonActivity::class.java)
            intent.putExtra("person_id", popularPersonList.results[position].id)
            holder.personDiv.context.startActivity(intent)
        }
    }

    class PopularPersonHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = PopularPersonItemBinding.bind(item)
        val personDiv = binding.personDiv
        val personName = binding.personName
        val personImg = binding.personImg
        val personPopularity = binding.popularityNum
        val rowPopularity = binding.rowPopularity
    }
}