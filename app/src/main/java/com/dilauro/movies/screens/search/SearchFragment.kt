package com.dilauro.movies.screens.search

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.GridLayoutManager
import com.dilauro.movies.R
import com.dilauro.movies.adapters.ResultSearchAdapter
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.SearchFragmentBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchFragment : Fragment() {

    private lateinit var viewModel: SearchViewModel
    private var _binding: SearchFragmentBinding? = null
    private val binding get() = _binding!!
    lateinit var resultSearchAdapter: ResultSearchAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SearchFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[SearchViewModel::class.java]
        (activity as AppCompatActivity).supportActionBar?.title = (activity as AppCompatActivity).getString(R.string.search_title)
        binding.searchInput.doOnTextChanged { _, _, _, _ ->
            getApiResult()
        }
    }

    private fun refreshSearchResult() {
        binding.resultSearchList.adapter = resultSearchAdapter
    }

    private fun getApiResult() {
        val inputText = binding.searchInput.text.toString()
        val apiServiceSearch = ApiService.create().getSearchResult(PRIVATE_API_KEY, CURRENT_LANG, inputText)
        apiServiceSearch.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {
                if (response.body() != null) {
                    resultSearchAdapter = response.body()?.let { ResultSearchAdapter(it, context!!) }!!
                    refreshSearchResult()
                    binding.resultSearchList.layoutManager = GridLayoutManager(context!!, 2)
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

}