package com.dilauro.movies.screens.calendar

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.dilauro.movies.R
import com.dilauro.movies.adapters.CalendarListMovieAdapter
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.CalendarFragmentBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CalendarFragment : Fragment() {

    private lateinit var viewModel: CalendarViewModel
    private var _binding: CalendarFragmentBinding? = null
    private val binding get() = _binding!!
    lateinit var calendarAdapter: CalendarListMovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = CalendarFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[CalendarViewModel::class.java]
        (activity as AppCompatActivity).supportActionBar?.title = (activity as AppCompatActivity).getString(R.string.upcoming_title)
        getApiDate()
    }

    private fun refreshCalendarList() {
        binding.releaseList.adapter = calendarAdapter
    }

    private fun getApiDate() {
        val apiServiceCalendar = ApiService.create().getNewCinemaMovies(PRIVATE_API_KEY, CURRENT_LANG, "RU")

        apiServiceCalendar.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {
                if (response.body() != null) {
                    calendarAdapter = context?.let { CalendarListMovieAdapter(response.body()!!, it) }!!
                    refreshCalendarList()
                    binding.releaseList.layoutManager = GridLayoutManager(context, 1)
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

}