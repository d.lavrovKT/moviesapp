package com.dilauro.movies.screens.toplist

import android.os.Build
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dilauro.movies.R
import com.dilauro.movies.adapters.TopListAdapter
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.movies.Movie
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.databinding.TopListFragmentBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TopListFragment : Fragment() {

    private lateinit var viewModel: TopListViewModel
    private var _binding: TopListFragmentBinding? = null
    private val binding get() = _binding!!
    lateinit var topListAdapter: TopListAdapter
    var currentPage = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = TopListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[TopListViewModel::class.java]
        (activity as AppCompatActivity).supportActionBar?.title = (activity as AppCompatActivity).getString(R.string.top_title)
        getApiDate()
        binding.TopList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPos = layoutManager.findLastVisibleItemPosition()
                val allItems = recyclerView.layoutManager?.itemCount
                if (allItems != null && currentPage < 26) {
                    if (lastPos == (allItems - 1)) {
                        currentPage++
                        updateApiDate()
                    }
                }
            }
        })
        // TODO: Use the ViewModel
    }

    fun refreshTopList() {
        binding.TopList.adapter = topListAdapter
    }

    private fun updateApiDate() {
        val apiServiceTopMovie = ApiService.create().getRateMovies(PRIVATE_API_KEY, CURRENT_LANG, currentPage)

        apiServiceTopMovie.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {
                response.body()?.results?.let { topListAdapter.updateItems(it) }
                topListAdapter.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun getApiDate() {
        val apiServiceTopMovie = ApiService.create().getRateMovies(PRIVATE_API_KEY, CURRENT_LANG, currentPage)

        apiServiceTopMovie.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {
                if (response.body() != null) {
                    topListAdapter = TopListAdapter(response.body()!!, context!!)
                    refreshTopList()
                    binding.TopList.layoutManager = GridLayoutManager(context, 2)
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}