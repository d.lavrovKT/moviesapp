package com.dilauro.movies.screens.favorites

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dilauro.movies.R
import com.dilauro.movies.adapters.FavoriteListAdapter
import com.dilauro.movies.adapters.database.DbHandler
import com.dilauro.movies.databinding.FavoritesFragmentBinding
import com.dilauro.movies.databinding.TopListFragmentBinding

class FavoritesFragment : Fragment() {

    private var _binding: FavoritesFragmentBinding? = null
    private val binding get() = _binding!!
    private var tabWatchState = true
    lateinit var dbHandler: DbHandler

    private lateinit var viewModel: FavoritesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FavoritesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[FavoritesViewModel::class.java]
        (activity as AppCompatActivity).supportActionBar?.title = (activity as AppCompatActivity).getString(R.string.favorites_title)
        dbHandler = DbHandler(context!!)
        binding.favoriteList.layoutManager = LinearLayoutManager(context)
        refreshFavoritesList(0)
        refreshFavoritesList(0)
        binding.selectLast.setOnClickListener {
            if (tabWatchState) {
                tabWatchState = false
                binding.selectFuture.alpha = 0.6F
                binding.selectFuture.setBackgroundResource(R.drawable.white_bg)
                binding.selectLast.alpha = 1F
                binding.selectLast.setBackgroundResource(R.drawable.border_bottom_line)
                refreshFavoritesList(1)
            }
        }
        binding.selectFuture.setOnClickListener {
            if (!tabWatchState) {
                tabWatchState = true
                binding.selectFuture.alpha = 1F
                binding.selectFuture.setBackgroundResource(R.drawable.border_bottom_line)
                binding.selectLast.alpha = 0.6F
                binding.selectLast.setBackgroundResource(R.drawable.white_bg)
                refreshFavoritesList(0)
            }
        }
    }

    fun refreshFavoritesList(state: Int) {
        binding.favoriteList.adapter = FavoriteListAdapter(dbHandler.getToFavoriteItem(state), this, state)
        val favoriteList = dbHandler.getToFavoriteItem(state)
        if (favoriteList.size == 0) {
            binding.emptyList.visibility = View.VISIBLE
        } else {
            binding.emptyList.visibility = View.GONE
        }
    }

}