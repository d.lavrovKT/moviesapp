package com.dilauro.movies.screens.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dilauro.movies.DetailMovieActivity
import com.dilauro.movies.R
import com.dilauro.movies.RussiaLocationActivity
import com.dilauro.movies.adapters.MoviesListAdapter
import com.dilauro.movies.adapters.PopularPeopleAdapter
import com.dilauro.movies.adapters.UpcomingAdapter
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.film.MovieItem
import com.dilauro.movies.data.movies.MoviesList
import com.dilauro.movies.data.person.PeopleList
import com.dilauro.movies.databinding.HomeFragmentBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random
import kotlin.random.nextInt

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private var _binding: HomeFragmentBinding? = null  //подключение viewbinding в фрагменте
    private val binding get() = _binding!! //подключение viewbinding в фрагменте
    lateinit var moviesListAdapter: MoviesListAdapter
    lateinit var upcomingAdapter: UpcomingAdapter
    lateinit var popularPersonAdapter: PopularPeopleAdapter
    lateinit var replaceDataValue : ReplaceDataValue

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        replaceDataValue = ReplaceDataValue(context!!)
        // TODO: Use the ViewModel
        getApiData()
        getRandomMovie()
        binding.getNewRandomMovie.setOnClickListener {
            getRandomMovie()
        }
        (activity as AppCompatActivity).supportActionBar?.title = (activity as AppCompatActivity).getString(R.string.main_title)
    }

    private fun refreshMoviesList() {
        binding.moviesList.adapter = moviesListAdapter
    }

    private fun refreshUpcomingList() {
        binding.upcomingList.adapter = upcomingAdapter
    }

    private fun refreshPopularPersonList() {
        binding.popularPersonList.adapter = popularPersonAdapter
    }


    private fun getApiData() {
        val apiServiceUpcoming = ApiService.create().getNewCinemaMovies(PRIVATE_API_KEY, CURRENT_LANG, "RU")
        val apiServiceMovies = ApiService.create().getMovies(PRIVATE_API_KEY, CURRENT_LANG)
        val apiServicePopularPersonList = ApiService.create().getPopularPerson(PRIVATE_API_KEY, CURRENT_LANG)

        apiServiceUpcoming.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {
                if (response.body() != null) {
                    upcomingAdapter = response.body()?.let { UpcomingAdapter(it) }!!
                    refreshUpcomingList()
                    binding.upcomingList.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
                } else {
                    val intent = Intent(activity, RussiaLocationActivity::class.java)
                    startActivity(intent)
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {
                val intent = Intent(activity, RussiaLocationActivity::class.java)
                startActivity(intent)
            }
        })

        apiServiceMovies.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {
                if (response.body() != null) {
                    moviesListAdapter = response.body()?.let { MoviesListAdapter(it, context!!) }!!
                    refreshMoviesList()
                    binding.moviesList.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
                } else {
                    val intent = Intent(activity, RussiaLocationActivity::class.java)
                    startActivity(intent)
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {
                val intent = Intent(activity, RussiaLocationActivity::class.java)
                startActivity(intent)
            }
        })


        apiServicePopularPersonList.enqueue(object : Callback<PeopleList> {
            override fun onResponse(call: Call<PeopleList>, response: Response<PeopleList>) {
                if (response.body() != null) {
                    popularPersonAdapter = response.body()?.let { PopularPeopleAdapter(it, context!!) }!!
                    refreshPopularPersonList()
                    binding.popularPersonList.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
                }
            }

            override fun onFailure(call: Call<PeopleList>, t: Throwable) {
                val intent = Intent(activity, RussiaLocationActivity::class.java)
                startActivity(intent)
            }
        })
    }

    private fun getRandomMovie() {
        val random = Random
        val randomPage = random.nextInt(1..40)
        val randomMovieInd = random.nextInt(0..19)
        val apiServiceRandomMovie = ApiService.create().getRandomMovie(randomPage, PRIVATE_API_KEY, CURRENT_LANG)

        apiServiceRandomMovie.enqueue(object : Callback<MoviesList> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {
                if (response.body() != null) {
                    val randomMovie = response.body()?.results?.get(randomMovieInd)

                    randomMovie?.let { ApiService.create().getMovieDetail(it.id, PRIVATE_API_KEY, CURRENT_LANG) }?.enqueue(object : Callback<MovieItem> {
                        override fun onResponse(call: Call<MovieItem>, response: Response<MovieItem>) {
                            binding.randomGenres.text = replaceDataValue.getYearDate(randomMovie.release_date) +
                                    if (response.body()?.genres?.size!! > 0) {
                                        ", " +
                                                (response.body()?.genres?.get(0)?.name ?: "") +
                                                if (response.body()?.genres?.size!! > 1) {
                                                    "/" + (response.body()?.genres?.get(1)?.name ?: "")
                                                } else {
                                                    ""
                                                }
                                    } else {
                                        ""
                                    }
                        }

                        override fun onFailure(call: Call<MovieItem>, t: Throwable) {
                            Toast.makeText(activity, (activity as AppCompatActivity).getString(R.string.fail_request), Toast.LENGTH_SHORT).show()
                        }
                    })
                    binding.randomTitle.text = randomMovie?.title
                    if (randomMovie?.vote_average != 0.0) {
                        binding.randomMovieRate.text = randomMovie?.vote_average.toString()
                    } else {
                        binding.rateLinear.visibility = View.INVISIBLE
                    }
                    binding.randomDescription.text = replaceDataValue.replaceMovieDescription(randomMovie?.overview.toString())
                    if (randomMovie?.backdrop_path != null) {
                        Picasso.get().load("https://images.tmdb.org/t/p/w500" + randomMovie.backdrop_path).into(binding.randomPoster)
                    } else if (randomMovie?.poster_path != null) {
                        Picasso.get().load("https://images.tmdb.org/t/p/w500" + randomMovie.poster_path).into(binding.randomPoster)
                    } else {
                        binding.randomPoster.setImageResource(R.drawable.no_image)
                    }

                    binding.randomPoster.setOnClickListener {
                        val intent = Intent(activity, DetailMovieActivity::class.java)
                        intent.putExtra("movie_id", randomMovie?.id)
                        startActivity(intent)
                    }
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {
                val intent = Intent(activity, RussiaLocationActivity::class.java)
                startActivity(intent)
            }
        })
    }
}