package com.dilauro.movies

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.dilauro.movies.adapters.CastListAdapter
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.moviepeople.Movie
import com.dilauro.movies.databinding.ActivityCastListBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CastListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCastListBinding
    private lateinit var castListAdapter: CastListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCastListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.movieTilte.text = intent.getStringExtra("movie_name")
        val movieId = intent.getIntExtra("movie_id", 0)
        getCastList()
        binding.btnBack.setOnClickListener {
            intent = Intent(this, DetailMovieActivity::class.java)
            intent.putExtra("movie_id", movieId)
            startActivity(intent)
        }
    }

    private fun getCastList() {
        val movieId = intent.getIntExtra("movie_id", 0)
        val apiServiceMoviePerson = ApiService.create().getMoviesWhitCredits(movieId, PRIVATE_API_KEY, CURRENT_LANG, "credits")

        apiServiceMoviePerson.enqueue(object : Callback<Movie> {
            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                if (response.body() != null) {
                    castListAdapter = response.body()?.let { CastListAdapter(it) }!!
                    refreshCastList()
                    binding.recyclerCastList.layoutManager = GridLayoutManager(this@CastListActivity, 1)
                }
            }

            override fun onFailure(call: Call<Movie>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun refreshCastList() {
        binding.recyclerCastList.adapter = castListAdapter
    }
}