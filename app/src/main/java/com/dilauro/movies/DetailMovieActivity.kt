package com.dilauro.movies

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.net.wifi.WifiManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.Formatter.formatIpAddress
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dilauro.movies.adapters.MoviePersonAdapter
import com.dilauro.movies.adapters.SimilarListAdapter
import com.dilauro.movies.adapters.StartYoutubeVideo
import com.dilauro.movies.adapters.database.DbHandler
import com.dilauro.movies.api.ApiService
import com.dilauro.movies.const.CURRENT_LANG
import com.dilauro.movies.const.PRIVATE_API_KEY
import com.dilauro.movies.data.dbclasses.FavoritesItem
import com.dilauro.movies.data.film.MovieItem
import com.dilauro.movies.data.moviepeople.Movie
import com.dilauro.movies.data.similar.Similar
import com.dilauro.movies.databinding.ActivityDetailMovieBinding
import com.dilauro.movies.replacedata.ReplaceDataValue
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class DetailMovieActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailMovieBinding
    lateinit var moviesPersonAdapter: MoviePersonAdapter
    lateinit var replaceDataValue: ReplaceDataValue
    private lateinit var startYoutubeVideo: StartYoutubeVideo
    lateinit var similarListAdapter: SimilarListAdapter
    lateinit var dbHandler: DbHandler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val movieId = intent.getIntExtra("movie_id", 0)
        dbHandler = DbHandler(this)
        replaceDataValue = ReplaceDataValue(this)
        getDetailMovie()
        getMoviePerson()
        binding.detailPageVideo.setOnClickListener {
            startYoutubeVideo = StartYoutubeVideo(this)
            startYoutubeVideo.startVideo(binding.videoId.text.toString().toInt(), binding.detailPageVideo)
        }

        binding.backImgBtn.setOnClickListener {
            startActivity(Intent(this, StartScreen::class.java))
        }

        val favoriteList = dbHandler.getFavoriteListId()
        if (favoriteList.contains(movieId)) {
            binding.btnAddFavorites.text = getString(R.string.to_favorites)
        }

    }

    private fun getDetailMovie() {
        val movieId = intent.getIntExtra("movie_id", 0)
        val apiServiceDetailMovie = ApiService.create().getMovieDetail(movieId, PRIVATE_API_KEY, CURRENT_LANG)
        val apiServiceSimilarMovies = ApiService.create().getSimilarMovies(movieId, PRIVATE_API_KEY, CURRENT_LANG)

        apiServiceDetailMovie.enqueue(object : Callback<MovieItem> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<MovieItem>, response: Response<MovieItem>) {
                binding.movieId.text = response.body()?.id.toString()
                binding.movieTitle.text = response.body()?.title
                if (response.body()?.overview != "") {
                    binding.descriptionText.text = response.body()?.overview
                } else {
                    binding.descriptionText.text = getString(R.string.missing_description)
                }
                binding.videoId.text = response.body()?.id.toString()
                if (response.body()?.release_date != "") {
                    binding.dateValue.text = response.body()?.release_date?.let { replaceDataValue.replaceDate(it) }
                } else {
                    binding.dateValue.text = getString(R.string.no_request)
                }

                if (response.body()?.original_language != "") {
                    binding.countryValue.text = response.body()?.original_language?.let { replaceDataValue.replaceLang(it) }
                } else {
                    binding.countryValue.text = getString(R.string.no_request)
                }

                if (response.body()?.budget != 0) {
                    binding.budgetValue.text = response.body()?.budget?.toString()?.let { replaceDataValue.replaceSum(it) }
                } else {
                    binding.budgetValue.text = getString(R.string.no_request)
                }

                if (response.body()?.revenue != 0) {
                    binding.revenueValue.text = response.body()?.revenue?.toString()?.let { replaceDataValue.replaceSum(it) }
                } else {
                    binding.revenueValue.text = getString(R.string.no_request)
                }

                if (response.body()?.homepage != "") {
                    binding.linkValue.text = getString(R.string.go_link)
                    binding.linkValue.setOnClickListener {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("${response.body()?.homepage}"))
                        startActivity(browserIntent)
                    }
                } else {
                    binding.linkValue.text = getString(R.string.no_request)
                    binding.linkImg.visibility = View.GONE
                }

                if (response.body()?.backdrop_path != null) {
                    Picasso.get().load("https://images.tmdb.org/t/p/w500" + response.body()?.backdrop_path).into(binding.detailPageVideo)
                } else {
                    binding.detailPageVideo.setImageResource(R.drawable.no_image)
                }

                if (response.body()?.backdrop_path != null) {
                    Picasso.get().load("https://images.tmdb.org/t/p/w500" + response.body()?.backdrop_path).into(binding.posterMovie)
                } else if (response.body()?.poster_path != null) {
                    Picasso.get().load("https://images.tmdb.org/t/p/w500" + response.body()?.poster_path).into(binding.posterMovie)
                } else {
                    binding.posterMovie.setImageResource(R.drawable.no_image)
                }

                binding.movieGenres.text = (response.body()?.genres?.get(0)?.name ?: "") +
                        if (response.body()?.genres?.size!! > 1) {
                            "/" + (response.body()?.genres?.get(1)?.name ?: "")
                        } else {
                            ""
                        }

                if (response.body()?.vote_average != 0.0) {
                    binding.movieRate.text = response.body()?.vote_average.toString()
                } else {
                    binding.rateLinear.visibility = View.INVISIBLE
                }
                binding.allPeopleBtn.setOnClickListener {
                    intent = Intent(this@DetailMovieActivity, CastListActivity::class.java)
                    intent.putExtra("movie_id", movieId)
                    intent.putExtra("movie_name", response.body()?.title)
                    startActivity(intent)
                }

                binding.btnAddFavorites.setOnClickListener {
                    val dialog = AlertDialog.Builder(this@DetailMovieActivity)
                    dialog.setTitle(R.string.add_question)
                    dialog.setPositiveButton(R.string.add) { _: DialogInterface, _: Int ->
                        val favoritesItem = FavoritesItem()
                        favoritesItem.itemId = response.body()?.id!!
                        favoritesItem.itemName = response.body()?.title.toString()
                        favoritesItem.itemRate = response.body()?.vote_average ?: 0.0
                        favoritesItem.itemPosterPath = response.body()?.poster_path.toString()
                        favoritesItem.itemBackPath = response.body()?.backdrop_path.toString()
                        favoritesItem.itemIsWatched = 0
                        dbHandler.addToFavoriteItem(favoritesItem)
                        binding.btnAddFavorites.text = getString(R.string.to_favorites)
                    }
                    dialog.setNegativeButton(R.string.cancel) { _: DialogInterface, _: Int ->
                    }
                    dialog.show()
                }
            }

            override fun onFailure(call: Call<MovieItem>, t: Throwable) {
                Toast.makeText(this@DetailMovieActivity, getString(R.string.fail_request), Toast.LENGTH_SHORT).show()
            }
        })

        apiServiceSimilarMovies.enqueue(object : Callback<Similar> {
            override fun onResponse(call: Call<Similar>, response: Response<Similar>) {
                if (response.body() != null) {
                    similarListAdapter = response.body()?.let { SimilarListAdapter(it, this@DetailMovieActivity) }!!
                    refreshSimilarList()
                    binding.similarMoviesList.layoutManager = LinearLayoutManager(this@DetailMovieActivity, LinearLayoutManager.HORIZONTAL, false)
                }
            }

            override fun onFailure(call: Call<Similar>, t: Throwable) {
                Toast.makeText(this@DetailMovieActivity, getString(R.string.fail_request), Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun refreshPersonList() {
        binding.actorsRW.adapter = moviesPersonAdapter
    }

    private fun getMoviePerson() {
        val movieId = intent.getIntExtra("movie_id", 0)
        val apiServiceMoviePerson = ApiService.create().getMoviesWhitCredits(movieId, PRIVATE_API_KEY, CURRENT_LANG, "credits")

        apiServiceMoviePerson.enqueue(object : Callback<Movie> {
            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                if (response.body() != null) {
                    moviesPersonAdapter = response.body()?.let { MoviePersonAdapter(it) }!!
                    refreshPersonList()
                    binding.actorsRW.layoutManager = GridLayoutManager(this@DetailMovieActivity, 2)
                }
            }

            override fun onFailure(call: Call<Movie>, t: Throwable) {
                Toast.makeText(this@DetailMovieActivity, getString(R.string.fail_request), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun refreshSimilarList() {
        binding.similarMoviesList.adapter = similarListAdapter
    }
}