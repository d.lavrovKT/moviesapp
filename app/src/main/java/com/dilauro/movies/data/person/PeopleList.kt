package com.dilauro.movies.data.person

data class PeopleList(
    val page: Int,
    val results: List<Person>,
    val total_pages: Int,
    val total_results: Int
)