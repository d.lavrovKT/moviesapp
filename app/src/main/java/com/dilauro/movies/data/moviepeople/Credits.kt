package com.dilauro.movies.data.moviepeople

data class Credits(
    val cast: List<Cast>,
    val crew: List<Crew>
)