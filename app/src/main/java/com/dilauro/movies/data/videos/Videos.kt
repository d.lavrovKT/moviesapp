package com.dilauro.movies.data.videos

data class Videos(
    val id: Int,
    val results: List<Result>
)