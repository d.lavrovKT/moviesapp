package com.dilauro.movies.data.dbclasses

class FavoritesItem() {
    var itemId: Int = -1
    var itemName: String = ""
    var itemRate: Double = 0.0
    var itemPosterPath: String = ""
    var itemBackPath: String = ""
    var itemIsWatched: Int = 0
}
