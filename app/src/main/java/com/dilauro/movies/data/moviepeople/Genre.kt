package com.dilauro.movies.data.moviepeople

data class Genre(
    val id: Int,
    val name: String
)