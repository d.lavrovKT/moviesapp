package com.dilauro.movies.data.film

data class Genre(
    val id: Int,
    val name: String
)