package com.dilauro.movies.data.film

data class SpokenLanguage(
    val english_name: String,
    val iso_639_1: String,
    val name: String
)