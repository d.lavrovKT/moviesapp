package com.dilauro.movies.data.film

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)