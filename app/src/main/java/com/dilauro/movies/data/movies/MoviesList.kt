package com.dilauro.movies.data.movies

import com.dilauro.movies.data.movies.Movie

data class MoviesList(
    val page: Int,
    val results: MutableList<Movie>,
    val total_pages: Int,
    val total_results: Int
)