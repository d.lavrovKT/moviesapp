package com.dilauro.movies.data.personmovies

data class PersonMoviesList(
    val cast: List<Cast>,
    val crew: List<Any>,
    val id: Int
)