package com.dilauro.movies.replacedata

import android.app.Activity
import android.app.Application
import android.content.Context
import android.provider.Settings.Secure.getString
import com.dilauro.movies.R
import com.dilauro.movies.const.CURRENT_LANG
import java.text.DecimalFormat

class ReplaceDataValue (val context: Context) {

    fun replaceLang(lang: String): String {
        if (CURRENT_LANG == "en") {
            return lang
        } else {
            return when (lang) {
                "en" -> "Английский"
                "ru" -> "Русский"
                "es" -> "Испанский"
                "sv" -> "Шведский"
                "fr" -> "Французский"
                "aa" -> "Афарский"
                "ab" -> "Абхазский"
                "af" -> "Африкаанс"
                "am" -> "Амхарский"
                "an" -> "Арагонский"
                "ar" -> "Арабский"
                "as" -> "Ассамский"
                "ay" -> "Аймарский"
                "az" -> "Азербайджанский"
                "ba" -> "Башкирский"
                "be" -> "Белорусский"
                "bg" -> "Болгарский"
                "bh" -> "Бихарский"
                "bi" -> "Бислама"
                "bn" -> "Бенгальский"
                "bo" -> "Тибетский"
                "br" -> "Бретонский"
                "ca" -> "Каталонский"
                "co" -> "Корсиканский"
                "cs" -> "Чешский"
                "cy" -> "Валлийский"
                "da" -> "Датский"
                "de" -> "Немецкий"
                "dz" -> "Бхутани"
                "el" -> "Греческий"
                "eo" -> "Эсперанто"
                "et" -> "Эстонский"
                "eu" -> "Баскский"
                "fa" -> "Фарси"
                "fi" -> "Финский"
                "fj" -> "Фиджи"
                "fo" -> "Фарерский"
                "fy" -> "Фризский"
                "ga" -> "Ирландский"
                "gd" -> "Гэльский"
                "gl" -> "Галисийский"
                "gn" -> "Гуарани"
                "gu" -> "Гуджарати"
                "ha" -> "Хауса"
                "he" -> "Еврейский"
                "hi" -> "Хинди"
                "hr" -> "Хорватский"
                "ht" -> "Гаитянский"
                "hu" -> "Венгерский"
                "hy" -> "Армянский"
                "ia" -> "Интерлингва"
                "id" -> "Индонезийский"
                "ie" -> "Интерлингва"
                "ii" -> "Сычуань И"
                "ik" -> "Инупиак"
                "io" -> "Идо"
                "is" -> "Исландский"
                "it" -> "Итальянский"
                "iu" -> "Инуктитут"
                "ja" -> "Японский"
                "jv" -> "Яванский"
                "ka" -> "Грузинский"
                "kk" -> "Казахский"
                "kl" -> "Гренландский"
                "km" -> "Камбоджийский"
                "kn" -> "Каннада"
                "ko" -> "Корейский"
                "ks" -> "Кашмирский"
                "ku" -> "Курдский"
                "ky" -> "Киргизский"
                "la" -> "Латинский"
                "li" -> "Лимбургский"
                "ln" -> "Лингала"
                "lo" -> "Лаосский"
                "lt" -> "Литовский"
                "lv" -> "Латвийский"
                "mg" -> "Малагасийский"
                "mi" -> "Маорийский"
                "mk" -> "Македонский"
                "ml" -> "Малаялам"
                "mn" -> "Монгольский"
                "mo" -> "Молдавский"
                "mr" -> "Маратхский"
                "ms" -> "Малайский"
                "mt" -> "Мальтийский"
                "my" -> "Бирманский"
                "na" -> "Науруанский"
                "ne" -> "Непальский"
                "nl" -> "Нидерландский"
                "no" -> "Норвежский"
                "oc" -> "Окситанский"
                "om" -> "Оромо"
                "or" -> "Ория"
                "pa" -> "Пенджабский"
                "pl" -> "Польский"
                "ps" -> "Пушту"
                "pt" -> "Португальский"
                "qu" -> "Кечуа"
                "rm" -> "Ретороманский"
                "rn" -> "Кирунди"
                "ro" -> "Румынский"
                "rw" -> "Киняруанда"
                "sa" -> "Санскритский"
                "sd" -> "Синдхи"
                "sg" -> "Сангро"
                "sh" -> "Сербо-Хорватский"
                "si" -> "Сингальский"
                "sk" -> "Словацкий"
                "sl" -> "Словенский"
                "sm" -> "Самоанский"
                "sn" -> "Шона"
                "so" -> "Сомалийский"
                "sq" -> "Албанский"
                "sr" -> "Сербский"
                "ss" -> "Свати"
                "st" -> "Северный сото"
                "su" -> "Сунданский"
                "sw" -> "Суахили"
                "ta" -> "Тамильский"
                "te" -> "Телугу"
                "tg" -> "Таджикский"
                "th" -> "Тайский"
                "ti" -> "Тигринья"
                "tk" -> "Туркменский"
                "tl" -> "Тагальский"
                "tn" -> "Тсвана"
                "to" -> "Тонга"
                "tr" -> "Турецкий"
                "ts" -> "Тсонга"
                "tt" -> "Татарский"
                "tw" -> "Чви"
                "ug" -> "Уйгурский"
                "uk" -> "Украинский"
                "ur" -> "Урду"
                "uz" -> "Узбекский"
                "vi" -> "Вьетнамский"
                "vo" -> "Волапюк"
                "wa" -> "Валлон"
                "wo" -> "Волоф"
                "xh" -> "Коса"
                "yi" -> "Идиш"
                "ji" -> "Идиш"
                "yo" -> "Йоруба"
                "zh" -> "Китайский"
                "zu" -> "Зулусский"
                else -> lang
            }
        }
    }

    fun replaceSum(sum: String): String {
        val sumReverse = sum.reversed()
        var convertSum = ""
        for (item in sumReverse.indices) {
            if ((item % 3 == 0) && (item != 0)) {
                convertSum += " " + sumReverse[item]
            } else {
                convertSum += sumReverse[item]
            }
        }
        return "${convertSum.reversed()}$"
    }

    private fun getConvertMonth(month: String): String {
        val convertMonth = when (month) {
            "01" -> context.getString(R.string.month_jan)
            "02" -> context.getString(R.string.month_feb)
            "03" -> context.getString(R.string.month_mar)
            "04" -> context.getString(R.string.month_apr)
            "05" -> context.getString(R.string.month_may)
            "06" -> context.getString(R.string.month_jun)
            "07" -> context.getString(R.string.month_jul)
            "08" -> context.getString(R.string.month_aug)
            "09" -> context.getString(R.string.month_sep)
            "10" -> context.getString(R.string.month_oct)
            "11" -> context.getString(R.string.month_nov)
            "12" -> context.getString(R.string.month_dec)
            else -> ""
        }
        return convertMonth
    }

    fun replaceDate(date: String): String {
        val year = date.substringBefore("-")
        val month = date.substringAfter("-").substringBefore("-")
        val number = date.substringAfter("-").substringAfter("-")
        val convertMonth = getConvertMonth(month)
        return "$number $convertMonth $year"
    }

    fun getCalendarDay(date: String): String {
        return date.substringAfter("-").substringAfter("-")
    }

    fun getCalendarMonth(date: String): String {
        val month = date.substringAfter("-").substringBefore("-")
        return getConvertMonth(month)
    }

    fun replaceLink(link: String): String {
        return link.substringAfter("//www.").substringBefore("/")
    }

    fun getYearDate(date: String): String {
        return date.substringBefore("-")
    }

    fun replacePopularity(rate: Double): String {
        val pattern = "##0.0"
        val decimalFormat = DecimalFormat(pattern)
        return decimalFormat.format(rate)
    }

    fun replaceMovieDescription(desc: String): String {
        return if (desc.length > 200) {
            val editDesc = desc.substring(0, 200)
            "$editDesc..."
        } else desc
    }
}

